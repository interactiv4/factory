<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Factory\Test;

use Interactiv4\Contracts\Factory\Api\ObjectFactoryInterface;
use Interactiv4\Factory\ObjectFactory;
use Interactiv4\Factory\Test\_files\MyClass;
use Interactiv4\Factory\Test\_files\MyClassInterface;
use Interactiv4\Factory\Test\_files\MyObject;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class ObjectFactoryTest.
 *
 * @internal
 */
class ObjectFactoryTest extends TestCase
{
    /**
     * @var ObjectFactory
     */
    private $objectFactory;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        $this->objectFactory = new ObjectFactory();
    }

    /**
     * Test ObjectFactory class exists, is an instance of ObjectFactoryInterface.
     */
    public function testGetInstance(): void
    {
        static::assertInstanceOf(ObjectFactoryInterface::class, ObjectFactory::getInstance());
    }

    /**
     * Test singleton instance.
     */
    public function testSingletonInstance(): void
    {
        $objectFactory1 = ObjectFactory::getInstance();
        $objectFactory2 = ObjectFactory::getInstance();

        static::assertSame($objectFactory1, $objectFactory2);
    }

    /**
     * Test ObjectFactory static setInstance method.
     */
    public function testSetInstance(): void
    {
        $initialObjectFactory = ObjectFactory::getInstance();
        /** @var ObjectFactoryInterface|MockObject $suppliedObjectFactory */
        $suppliedObjectFactory = $this->getMockForAbstractClass(ObjectFactoryInterface::class);

        static::assertSame($initialObjectFactory, ObjectFactory::getInstance());

        ObjectFactory::setInstance($suppliedObjectFactory);

        static::assertSame($suppliedObjectFactory, ObjectFactory::getInstance());
        static::assertNotSame($initialObjectFactory, ObjectFactory::getInstance());
    }

    /**
     * Test create without mandatory arguments, only type.
     */
    public function testCreateWithoutMandatoryArguments(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $this->objectFactory->create(MyClass::class);
    }

    /**
     * Test create with missing mandatory, but autoinstantiable, parameters.
     */
    public function testCreateMissingMandatoryInstantiableArguments(): void
    {
        $myClass = $this->objectFactory->create(
            MyClass::class,
            [
                'stringVariable' => '1',
                'intVariable' => 1,
            ]
        );
        static::assertInstanceOf(MyClass::class, $myClass);
    }

    /**
     * Test create invalid arguments.
     */
    public function testCreateInvalidArguments(): void
    {
        $this->expectException(\TypeError::class);

        $this->objectFactory->create(
            MyClass::class,
            [
                'stringVariable' => '1',
                'intVariable' => 1,
                // This should be a MyObject instance
                'myObjectVariable' => 3,
            ]
        );
    }

    /**
     * Test create properly with valid arguments.
     */
    public function testCreateProperlyWithValidArguments(): void
    {
        $myClass = $this->objectFactory->create(
            MyClass::class,
            [
                'stringVariable' => '1',
                'intVariable' => 1,
                'myObjectVariable' => new MyObject(),
            ]
        );

        static::assertInstanceOf(MyClass::class, $myClass);
        static::assertInstanceOf(MyClassInterface::class, $myClass);
    }

    /**
     * Test create properly with extra unused arguments.
     */
    public function testCreateProperlyWithExtraUnusedArguments(): void
    {
        $myClass = $this->objectFactory->create(
            MyClass::class,
            [
                'stringVariable' => '1',
                'intVariable' => 1,
                'myObjectVariable' => new MyObject(),
                'nonExistingParamInConstructor' => 'dummy_value',
            ]
        );

        static::assertInstanceOf(MyClass::class, $myClass);
        static::assertInstanceOf(MyClassInterface::class, $myClass);
    }
}
