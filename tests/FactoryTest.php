<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Factory\Test;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;
use Interactiv4\Contracts\Factory\Api\ObjectFactoryInterface;
use Interactiv4\Factory\Factory;
use Interactiv4\Factory\ObjectFactory;
use Interactiv4\Factory\Test\_files\MyClass;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class FactoryTest.
 *
 * @internal
 */
class FactoryTest extends TestCase
{
    /**
     * @var string
     */
    private $type = MyClass::class;

    /**
     * @var Factory
     */
    private $factory;

    /**
     * @var ObjectFactoryInterface|MockObject
     */
    private $objectFactory;

    /**
     * {@inheritdoc}
     */
    protected function setUp(): void
    {
        /* @var ObjectFactoryInterface|MockObject $objectFactory */
        $this->objectFactory = $this->getMockForAbstractClass(ObjectFactoryInterface::class);
        $this->factory = new Factory($this->type, $this->objectFactory);
    }

    /**
     * Test Factory class exists and is an instance of FactoryInterface.
     */
    public function testInstanceOf(): void
    {
        static::assertInstanceOf(FactoryInterface::class, $this->factory);
    }

    /**
     * Test factory delegates creation to object factory.
     */
    public function testDelegatesCreationToObjectFactory(): void
    {
        $createArguments = [
            'createArgument1' => true,
            'createArgument2' => 2,
        ];

        $this->objectFactory->expects(static::once())
            ->method('create')
            ->with($this->type, $createArguments)
        ;

        $this->factory->create($createArguments);
    }
}
