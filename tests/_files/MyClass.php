<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Factory\Test\_files;

/**
 * Class MyClass.
 */
class MyClass implements MyClassInterface
{
    const STRING_VALUE = 'string_value';

    const INT_VALUE = 47;

    /**
     * MyClass constructor.
     *
     * @param string        $stringVariable
     * @param string        $mandatoryStringVariableConstantDefaultValue
     * @param int           $intVariable
     * @param int           $mandatoryIntVariableConstantDefaultValue
     * @param MyObject      $myObjectVariable
     * @param AnotherClass  $anotherClassVariable
     * @param string|null   $optionalStringVariableNull
     * @param int|null      $intVariableNull
     * @param MyObject|null $myObjectVariableNull
     * @param string        $optionalStringVariableTypedDefaultValue
     * @param int           $optionalIntVariableTypedDefaultValue
     * @param string        $optionalStringVariableConstantDefaultValue
     * @param int           $optionalIntVariableConstantDefaultValue
     */
    public function __construct(
        string $stringVariable,
        string $mandatoryStringVariableConstantDefaultValue,
        int $intVariable,
        int $mandatoryIntVariableConstantDefaultValue,
        MyObject $myObjectVariable,
        AnotherClass $anotherClassVariable,

        string $optionalStringVariableNull = null,
        int $intVariableNull = null,
        MyObject $myObjectVariableNull = null,

        string $optionalStringVariableTypedDefaultValue = '',
        int $optionalIntVariableTypedDefaultValue = 0,

        string $optionalStringVariableConstantDefaultValue = self::STRING_VALUE,
        int $optionalIntVariableConstantDefaultValue = self::INT_VALUE
    ) {
    }
}
