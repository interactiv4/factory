<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\Factory\Test\_files;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;
use Interactiv4\Factory\Factory;

/**
 * Class MyObjectFactory.
 */
class MyObjectFactory extends Factory implements FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        parent::__construct(MyObject::class);
    }
}
