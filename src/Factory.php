<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;
use Interactiv4\Contracts\Factory\Api\ObjectFactoryInterface;

/**
 * Class Factory.
 *
 * @api
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
class Factory implements FactoryInterface
{
    /**
     * Class name.
     *
     * @var string
     */
    private $type;

    /**
     * @var ObjectFactoryInterface
     */
    private $objectFactory;

    /**
     * Factory constructor.
     *
     * @param string                      $type
     * @param ObjectFactoryInterface|null $objectFactory
     */
    public function __construct(
        string $type,
        ObjectFactoryInterface $objectFactory = null
    ) {
        $this->type = $type;
        $this->objectFactory = $objectFactory ?? ObjectFactory::getInstance();
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $arguments = [])
    {
        return $this->objectFactory->create($this->type, $arguments);
    }
}
