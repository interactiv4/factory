<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory;

use Interactiv4\Factory\Api\ClassNameResolverInterface;
use InvalidArgumentException;

/**
 * Class ClassNameResolver.
 *
 * @api
 */
class ClassNameResolver implements ClassNameResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): string
    {
        $type = $arguments[self::ARGUMENTS_KEY_TYPE] ?? '';

        if (!\is_string($type)) {
            throw new InvalidArgumentException('Type must be a string');
        }

        if (0 === \strlen($type)) {
            throw new InvalidArgumentException('Type cannot be empty');
        }

        return $type;
    }
}
