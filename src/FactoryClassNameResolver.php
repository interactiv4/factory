<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;
use Interactiv4\Factory\Api\FactoryClassNameResolverInterface;
use InvalidArgumentException;

/**
 * Class FactoryClassNameResolver.
 *
 * @api
 */
class FactoryClassNameResolver implements FactoryClassNameResolverInterface
{
    /**
     * @var string[]
     */
    private $factoryClassNames;

    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): string
    {
        $type = $arguments[self::ARGUMENTS_KEY_TYPE] ?? '';

        if (!\is_string($type)) {
            throw new InvalidArgumentException('Type must be a string');
        }

        if (0 === \strlen($type)) {
            throw new InvalidArgumentException('Type cannot be empty');
        }

        if (!isset($this->factoryClassNames[$type])) {
            $this->factoryClassNames[$type] = $this->resolveFactoryClassName($type);
        }

        return $this->factoryClassNames[$type];
    }

    /**
     * @param string $type
     *
     * @return string
     */
    private function resolveFactoryClassName(string $type): string
    {
        $conventionFactoryClass = $type . 'Factory';

        return $this->checkFactoryClassName($conventionFactoryClass) ? $conventionFactoryClass : Factory::class;
    }

    /**
     * Check factory class.
     *
     * @param string $factoryClass
     *
     * @return bool
     */
    private function checkFactoryClassName(string $factoryClass): bool
    {
        return !empty($factoryClass)
            && \class_exists($factoryClass)
            && (
                \is_a(
                    $factoryClass,
                    FactoryInterface::class,
                    true
                )
            );
    }
}
