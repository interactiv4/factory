<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory\Parameter;

use Interactiv4\Factory\Api\Parameter\ParameterTypeNameResolverInterface;
use InvalidArgumentException;
use ReflectionType;

/**
 * Class ParameterTypeNameResolver.
 *
 * @api
 */
class ParameterTypeNameResolver implements ParameterTypeNameResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): string
    {
        $parameterType = $arguments[self::ARGUMENTS_KEY_PARAMETER_TYPE] ?? null;

        if (!$parameterType instanceof ReflectionType) {
            throw new InvalidArgumentException(\sprintf('Invalid parameter at %s', self::ARGUMENTS_KEY_PARAMETER_TYPE));
        }

        return (string) $parameterType;
    }
}
