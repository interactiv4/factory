<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory\Parameter;

use Interactiv4\Factory\Api\FactoryResolverInterface;
use Interactiv4\Factory\Api\Parameter\ParameterFactoryInterface;
use Interactiv4\Factory\Api\Parameter\ParameterTypeNameResolverInterface;
use Interactiv4\Factory\FactoryResolver;
use InvalidArgumentException;
use ReflectionParameter;

/**
 * Class ParameterResolver.
 *
 * @api
 */
class ParameterFactory implements ParameterFactoryInterface
{
    /**
     * @var ParameterTypeNameResolverInterface
     */
    private $parameterTypeNameResolver;

    /**
     * @var FactoryResolverInterface
     */
    private $factoryResolver;

    /**
     * ParameterFactory constructor.
     *
     * @param ParameterTypeNameResolverInterface|null $parameterTypeNameResolver
     * @param FactoryResolverInterface|null           $factoryResolver
     */
    public function __construct(
        ParameterTypeNameResolverInterface $parameterTypeNameResolver = null,
        FactoryResolverInterface $factoryResolver = null
    ) {
        $this->parameterTypeNameResolver = $parameterTypeNameResolver ?? new ParameterTypeNameResolver();
        $this->factoryResolver = $factoryResolver ?? new FactoryResolver();
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $arguments = [])
    {
        $parameter = $arguments[self::ARGUMENTS_KEY_PARAMETER] ?? null;

        if (!$parameter instanceof ReflectionParameter) {
            throw new InvalidArgumentException(\sprintf('Invalid parameter at %s', self::ARGUMENTS_KEY_PARAMETER));
        }

        $constructorArguments = $arguments[self::ARGUMENTS_KEY_ARGUMENTS] ?? [];

        if (!\is_array($constructorArguments)) {
            throw new InvalidArgumentException(\sprintf('Invalid arguments at %s', self::ARGUMENTS_KEY_ARGUMENTS));
        }

        // Resolve from supplied arguments, if available
        if (\array_key_exists($parameter->getName(), $constructorArguments)) {
            return $constructorArguments[$parameter->getName()];
        }

        // Resolve optional parameter
        if ($parameter->isOptional()) {
            return $this->resolveOptionalParameter($parameter);
        }

        // Resolve mandatory parameter
        if (!$parameter->isOptional()) {
            return $this->resolveMandatoryParameter($parameter);
        }

        // Unsupported case
        throw new InvalidArgumentException(\sprintf('Unsupported case for parameter %s', $parameter->getName()));
    }

    /**
     * @param ReflectionParameter $parameter
     *
     * @throws InvalidArgumentException
     *
     * @return mixed
     */
    private function resolveOptionalParameter(
        ReflectionParameter $parameter
    ) {
        // Use default value, if available
        if ($parameter->isDefaultValueAvailable()) {
            return $parameter->getDefaultValue();
        }

        // Otherwise use null, if allowed
        if ($parameter->allowsNull()) {
            return null;
        }

        // Unsupported case
        $errorMessage = \sprintf(
            'Unsupported case for optional parameter %s',
            $parameter->getName()
        );

        throw new InvalidArgumentException($errorMessage);
    }

    /**
     * @param ReflectionParameter $parameter
     *
     * @throws InvalidArgumentException
     *
     * @return mixed
     */
    private function resolveMandatoryParameter(
        ReflectionParameter $parameter
    ) {
        // Use default value, if available
        if ($parameter->isDefaultValueAvailable()) {
            return $parameter->getDefaultValue();
        }

        // When mandatory parameter has no type, it MUST be supplied
        if (!$parameter->hasType()) {
            $errorMessage = \sprintf(
                'Unable to instantiate mandatory parameter %s without type',
                $parameter->getName()
            );
            throw new InvalidArgumentException($errorMessage);
        }

        // When mandatory parameter is a built-in type, it MUST be supplied
        if ($parameter->getType()->isBuiltin()) {
            $errorMessage = \sprintf(
                'Unable to instantiate mandatory parameter %s with built-in type %s',
                $parameter->getName(),
                $this->getParameterTypeName($parameter->getType())
            );
            throw new InvalidArgumentException($errorMessage);
        }

        // At this point we have a mandatory, typed, non built-in parameter
        $parameterType = $this->getParameterTypeName($parameter->getType());

        // Class/Interface MUST exist
        if (!\class_exists($parameterType) && !\interface_exists($parameterType)) {
            $errorMessage = \sprintf(
                'Unable to instantiate mandatory parameter %s: class/interface %s does not exist',
                $parameter->getName(),
                $parameterType
            );
            throw new InvalidArgumentException($errorMessage);
        }

        // Try to resolve parameter type factory, to resolve recursively constructor parameters
        // Cannot provide current arguments to object factory, as dependent object may have
        // arguments with same name and different types
        // Custom factories are responsible to instantiate mandatory, non-autoinstantiable, parameters
        $factoryResolverArguments = [FactoryResolverInterface::ARGUMENTS_KEY_TYPE => $parameterType];

        return $this->factoryResolver->resolve($factoryResolverArguments)->create();
    }

    /**
     * Shortcut utility to call parameter type name resolver.
     *
     * @param \ReflectionType $parameterType
     *
     * @return string
     */
    private function getParameterTypeName(\ReflectionType $parameterType): string
    {
        return $this->parameterTypeNameResolver->resolve(
            [
                ParameterTypeNameResolverInterface::ARGUMENTS_KEY_PARAMETER_TYPE => $parameterType,
            ]
        );
    }
}
