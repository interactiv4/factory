<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory\Parameter;

use Interactiv4\Factory\Api\Parameter\ParameterFactoryInterface;
use Interactiv4\Factory\Api\Parameter\ParameterResolverInterface;
use Interactiv4\Factory\Api\Parameter\ParameterTypeNameResolverInterface;
use ReflectionParameter;

/**
 * Class ParameterResolver.
 *
 * @api
 */
class ParameterResolver implements ParameterResolverInterface
{
    /**
     * @var object[]
     */
    private $sharedInstances = [];

    /**
     * @var ParameterTypeNameResolverInterface
     */
    private $parameterTypeNameResolver;

    /**
     * @var ParameterFactoryInterface
     */
    private $parameterFactory;

    /**
     * ParameterResolver constructor.
     *
     * @param ParameterTypeNameResolverInterface|null $parameterTypeNameResolver
     * @param ParameterFactoryInterface|null          $parameterFactory
     */
    public function __construct(
        ParameterTypeNameResolverInterface $parameterTypeNameResolver = null,
        ParameterFactoryInterface $parameterFactory = null
    ) {
        $this->parameterTypeNameResolver = $parameterTypeNameResolver ?? new ParameterTypeNameResolver();
        $this->parameterFactory = $parameterFactory ?? new ParameterFactory();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = [])
    {
        return $this->resolveShared($arguments) ?? $this->resolveCreate($arguments);
    }

    /**
     * Resolve parameter using shared instances.
     *
     * @param array $arguments
     *
     * @return mixed|null
     */
    private function resolveShared(array $arguments = [])
    {
        $shared = $arguments[self::ARGUMENTS_KEY_SHARED] ?? false;

        if (!(bool) $shared) {
            return null;
        }

        $parameter = $arguments[self::ARGUMENTS_KEY_PARAMETER] ?? null;

        if (!$parameter instanceof ReflectionParameter || !$parameter->hasType()) {
            return null;
        }

        $parameterType = $this->getParameterTypeName($parameter->getType());

        if (!\array_key_exists($parameterType, $this->sharedInstances)) {
            $this->sharedInstances[$parameterType] = $this->resolveCreate($arguments);
        }

        return $this->sharedInstances[$parameterType];
    }

    /**
     * @param array $arguments
     *
     * @return mixed|object|null
     */
    private function resolveCreate(array $arguments = [])
    {
        return $this->parameterFactory->create(
            [
                ParameterFactoryInterface::ARGUMENTS_KEY_ARGUMENTS => $arguments[self::ARGUMENTS_KEY_ARGUMENTS] ?? null,
                ParameterFactoryInterface::ARGUMENTS_KEY_PARAMETER => $arguments[self::ARGUMENTS_KEY_PARAMETER] ?? null,
            ]
        );
    }

    /**
     * Shortcut utility to call parameter type name resolver.
     *
     * @param \ReflectionType $parameterType
     *
     * @return string
     */
    private function getParameterTypeName(\ReflectionType $parameterType): string
    {
        return $this->parameterTypeNameResolver->resolve(
            [
                ParameterTypeNameResolverInterface::ARGUMENTS_KEY_PARAMETER_TYPE => $parameterType,
            ]
        );
    }
}
