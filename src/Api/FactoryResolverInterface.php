<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory\Api;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;
use Interactiv4\Contracts\Resolver\Api\ResolverInterface;

/**
 * Interface FactoryResolver.
 *
 * @api
 */
interface FactoryResolverInterface extends ResolverInterface
{
    const ARGUMENTS_KEY_TYPE = 'type';

    /**
     * {@inheritdoc}
     *
     * @return FactoryInterface
     */
    public function resolve(array $arguments = []): FactoryInterface;
}
