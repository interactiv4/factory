<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory\Api;

use Interactiv4\Contracts\Resolver\Api\Resolver\StringResolverInterface;

/**
 * Interface FactoryClassNameResolverInterface.
 *
 * @api
 */
interface FactoryClassNameResolverInterface extends StringResolverInterface
{
    const ARGUMENTS_KEY_TYPE = 'type';

    /**
     * Resolve factory class name.
     *
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): string;
}
