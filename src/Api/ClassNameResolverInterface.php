<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory\Api;

use Interactiv4\Contracts\Resolver\Api\Resolver\StringResolverInterface;

/**
 * Interface ClassNameResolverInterface.
 *
 * @api
 */
interface ClassNameResolverInterface extends StringResolverInterface
{
    const ARGUMENTS_KEY_TYPE = 'type';

    /**
     * Resolve class name.
     *
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): string;
}
