<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory\Api\Parameter;

use Interactiv4\Contracts\Resolver\Api\Resolver\StringResolverInterface;

/**
 * Interface ParameterTypeNameResolverInterface.
 *
 * @api
 */
interface ParameterTypeNameResolverInterface extends StringResolverInterface
{
    const ARGUMENTS_KEY_PARAMETER_TYPE = 'parameter_type';

    /**
     * Resolve parameter type name.
     *
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): string;
}
