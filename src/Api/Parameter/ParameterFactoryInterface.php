<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory\Api\Parameter;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;

/**
 * Interface ParameterFactoryInterface.
 *
 * @api
 */
interface ParameterFactoryInterface extends FactoryInterface
{
    const ARGUMENTS_KEY_PARAMETER = 'parameter';

    const ARGUMENTS_KEY_ARGUMENTS = 'arguments';

    /**
     * Create parameter value.
     *
     * {@inheritdoc}
     *
     * @return object|mixed|null
     */
    public function create(array $arguments = []);
}
