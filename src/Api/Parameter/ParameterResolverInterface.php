<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory\Api\Parameter;

use Interactiv4\Contracts\Resolver\Api\ResolverInterface;

/**
 * Interface ParamResolverInterface.
 *
 * @api
 */
interface ParameterResolverInterface extends ResolverInterface
{
    const ARGUMENTS_KEY_PARAMETER = 'parameter';

    const ARGUMENTS_KEY_ARGUMENTS = 'arguments';

    const ARGUMENTS_KEY_SHARED = 'shared';

    /**
     * Resolve parameter value.
     *
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []);
}
