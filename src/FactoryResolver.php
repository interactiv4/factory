<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;
use Interactiv4\Factory\Api\FactoryClassNameResolverInterface;
use Interactiv4\Factory\Api\FactoryResolverInterface;
use Interactiv4\Factory\Api\Parameter\ParameterTypeNameResolverInterface;
use Interactiv4\Factory\Parameter\ParameterTypeNameResolver;
use InvalidArgumentException;
use ReflectionClass;

/**
 * Class FactoryResolver.
 *
 * @api
 */
class FactoryResolver implements FactoryResolverInterface
{
    /**
     * @var FactoryInterface[]
     */
    private $factories;

    /**
     * @var FactoryClassNameResolverInterface
     */
    private $factoryClassNameResolver;

    /**
     * @var ParameterTypeNameResolverInterface
     */
    private $parameterTypeNameResolver;

    /**
     * FactoryResolver constructor.
     *
     * @param FactoryClassNameResolverInterface|null  $factoryClassNameResolver
     * @param ParameterTypeNameResolverInterface|null $parameterTypeNameResolver
     */
    public function __construct(
        FactoryClassNameResolverInterface $factoryClassNameResolver = null,
        ParameterTypeNameResolverInterface $parameterTypeNameResolver = null
    ) {
        $this->factoryClassNameResolver = $factoryClassNameResolver ?? new FactoryClassNameResolver();
        $this->parameterTypeNameResolver = $parameterTypeNameResolver ?? new ParameterTypeNameResolver();
    }

    /**
     * Set factory.
     *
     * @param string           $type
     * @param FactoryInterface $factory
     */
    public function setFactory(
        string $type,
        FactoryInterface $factory
    ): void {
        $this->factories[$type] = $factory;
    }

    /**
     * Unset factory.
     *
     * @param string $type
     */
    public function unsetFactory(string $type): void
    {
        unset($this->factories[$type]);
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(array $arguments = []): FactoryInterface
    {
        $type = $arguments[self::ARGUMENTS_KEY_TYPE] ?? '';

        if (!\is_string($type)) {
            throw new InvalidArgumentException('Type must be a string');
        }

        if (0 === \strlen($type)) {
            throw new InvalidArgumentException('Type cannot be empty');
        }

        if (!isset($this->factories[$type])) {
            $factoryClass = $this->factoryClassNameResolver->resolve(
                [
                    FactoryClassNameResolverInterface::ARGUMENTS_KEY_TYPE => $type,
                ]
            );

            $constructorParams = [];
            $reflectionClass = new ReflectionClass($factoryClass);
            $constructor = $reflectionClass->getConstructor();

            if ($constructor) {
                foreach ($constructor->getParameters() as $parameter) {
                    // Inject type in factory class at the first string available constructor argument
                    if (!$parameter->hasType()) {
                        continue;
                    }

                    $parameterTypeNameResolverArguments = [
                        ParameterTypeNameResolverInterface::ARGUMENTS_KEY_PARAMETER_TYPE => $parameter->getType(),
                    ];

                    if ('string' !== $this->parameterTypeNameResolver->resolve($parameterTypeNameResolverArguments)) {
                        continue;
                    }

                    $constructorParams[$parameter->getName()] = $type;
                    break;
                }
            }

            $this->factories[$type] = $reflectionClass->newInstanceArgs($constructorParams);
        }

        return $this->factories[$type];
    }
}
