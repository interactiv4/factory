<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Factory;

use Interactiv4\Contracts\Factory\Api\ObjectFactoryInterface;
use Interactiv4\Factory\Api\ClassNameResolverInterface;
use Interactiv4\Factory\Api\Parameter\ParameterResolverInterface;
use Interactiv4\Factory\Parameter\ParameterResolver;
use InvalidArgumentException;
use ReflectionClass;

/**
 * Class ObjectFactory.
 *
 * @api
 */
class ObjectFactory implements ObjectFactoryInterface
{
    /**
     * @var ObjectFactoryInterface
     */
    private static $instance;

    /**
     * @var ParameterResolverInterface
     */
    private $parameterResolver;

    /**
     * @var ClassNameResolverInterface
     */
    private $classNameResolver;

    /**
     * @var bool
     */
    private $sharedParameters;

    /**
     * ObjectFactory constructor.
     *
     * @param ParameterResolverInterface|null $parameterResolver
     * @param ClassNameResolverInterface|null $classNameResolver
     * @param bool|null                       $sharedParameters
     */
    public function __construct(
        ParameterResolverInterface $parameterResolver = null,
        ClassNameResolverInterface $classNameResolver = null,
        bool $sharedParameters = null
    ) {
        $this->parameterResolver = $parameterResolver ?? new ParameterResolver();
        $this->sharedParameters = $sharedParameters ?? true;
        $this->classNameResolver = $classNameResolver ?? new ClassNameResolver();

        self::setInstance($this);
    }

    /**
     * Retrieve object factory.
     *
     * @throws \RuntimeException
     *
     * @return ObjectFactoryInterface
     */
    public static function getInstance(): ObjectFactoryInterface
    {
        if (!self::$instance instanceof ObjectFactoryInterface) {
            throw new \RuntimeException('ObjectFactory isn\'t initialized');
        }

        return self::$instance;
    }

    /**
     * Set object factory.
     *
     * @param ObjectFactoryInterface $instance
     */
    public static function setInstance(ObjectFactoryInterface $instance): void
    {
        self::$instance = $instance;
    }

    /**
     * {@inheritdoc}
     */
    public function create(
        string $type,
        array $arguments = []
    ) {
        if (0 === \strlen($type)) {
            throw new InvalidArgumentException('Type cannot be empty');
        }

        $constructorParams = [];
        $className = $this->classNameResolver->resolve(
            [
                ClassNameResolverInterface::ARGUMENTS_KEY_TYPE => $type,
            ]
        );
        $reflectionClass = new ReflectionClass($className);
        $constructor = $reflectionClass->getConstructor();

        if ($constructor) {
            foreach ($constructor->getParameters() as $parameter) {
                $paramResolverArguments = [
                    ParameterResolverInterface::ARGUMENTS_KEY_PARAMETER => $parameter,
                    ParameterResolverInterface::ARGUMENTS_KEY_ARGUMENTS => $arguments,
                    ParameterResolverInterface::ARGUMENTS_KEY_SHARED => $this->sharedParameters,
                ];

                $constructorParams[$parameter->getName()] = $this->parameterResolver->resolve($paramResolverArguments);
            }
        }

        return $reflectionClass->newInstanceArgs($constructorParams);
    }
}
